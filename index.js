let username;
let password;
let role;


// login function 
function login(){
	username = prompt("Enter your username:").toLowerCase();
	password = prompt("Enter your password:").toLowerCase();
	role = prompt("What is your role?").toLowerCase();

	// if input is empty exit function
	if (username === ''|| username == null || password === '' || password == null || role === '' || role == null){
		alert("Input should not be empty");
	}
	// if input is not empty, welcome user
	else {
		switch(role){
			case 'student':
				console.log("Welcome to the class portal, student!");		
				break;
			case 'admin':
				console.log("Welcome back to the class portal, admin!");		
				break;
			case 'teacher':
				console.log("Thank you for logging in, teacher!");
				break;
			default: 
				console.log("Role out of range.");
				break;
		}
		// invoke checkAverage function
		checkAverage();	
	}
}


// average grade function
function checkAverage(grade1, grade2, grade3, grade4){

	let averageScore = Math.round((grade1 + grade2 + grade3 + grade4) / arguments.length);


	if (username === ''|| username == null || password === '' || password == null || role === '' || role == null){
		alert("Input should not be empty");
	}else{

		if (role != 'student'){
			alert("You are not allowed to access this feature!")
		}
		else{
			if (averageScore<= 74){
				console.log("Hello, student, your average is: " + averageScore + ". The letter equivalent is F")
			}
			else if (averageScore >= 75 && averageScore <= 79){
				console.log("Hello, student, your average is: " + averageScore + ". The letter equivalent is D")
			} 
			else if (averageScore >= 80 && averageScore <= 84){
				console.log("Hello, student, your average is: " + averageScore + ". The letter equivalent is C")
			} 
			else if (averageScore >= 85 && averageScore <= 89){
				console.log("Hello, student, your average is: " + averageScore + ". The letter equivalent is B")
			}
			else if (averageScore >= 90 && averageScore <= 95){
				console.log("Hello, student, your average is: " + averageScore + ". The letter equivalent is A")
			}
			else if (averageScore >= 96){
				console.log("Hello, student, your average is: " + averageScore + ". The letter equivalent is A+")
			}		
		}
	}
}

login();